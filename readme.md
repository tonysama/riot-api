## What I've learnt while building the Riot API Application

1. I did not read through the Riot API data structure before building the application,
   which caused trouble while I need to fetch same URL with different search value.
   give an example: after I fetched recent matches with accountId, I need to fetch multiple
   match details.

2. I did not comment out those APIs that are tested, which caused reaching the API fetching limitation.

3. I did not plan the whole application development time properly, I focus too much of getting the data and
   data images from API and waste a lot of time, other than building the list of match histories.