let express = require('express');
let app = express();
let hbs = require('express-handlebars');
let request = require('request');
let async = require('async');
let _ = require('lodash');

// hbs.registerPartial(__dirname + '/views/layouts')

app.engine('handlebars', hbs({Layout: 'main'}));
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'handlebars');

/**
 * HomePage
 * 1st version only
 */
app.get('/', (req, res) => {
   res.render('index');
});

app.get('/search', (req, res) => {

    let data = {};
    let api_key = 'RGAPI-9ea28b1e-a8c0-42ca-a67e-e9ff8aec107d';
    let search_name = req.query.summoner.toLowerCase();
    let url = `https://na1.api.riotgames.com/lol/summoner/v3/summoners/by-name/${search_name}?api_key=${api_key}`;


    async.waterfall([

        /**
         * Function: get basic user info
         * @return:
         *  1.Summoner's name(case sensitive)
         *  2.Profile icon id
         *  3.Summoner's id(for future search)
         */
        (callback) => {
            request(url, (err, response, body) => {
                 if(!err && response.statusCode === 200){
                     let json = JSON.parse(body);
                     data.name = json.name;
                     data.icon = json.profileIconId;
                     data.id = json.id;
                     data.accountId = json.accountId;
                     callback(null, data);
                 }else{
                     console.log('error');
                 }
            });
        },

        /**
         * Function: get summoner's leagues ranking
         * @return: summoner leagues rankings and data
         */
        (data, callback) => {
            url = `https://na1.api.riotgames.com/lol/league/v3/positions/by-summoner/${data.id}?api_key=${api_key}`;
            request(url, (err, response, body) => {
                if(!err && response.statusCode === 200){
                    data.leagues = JSON.parse(body);
                    /**
                     * Taking tier images from OP.GG, they are naming images in lower case (sad face)
                     */
                    data.leagues.forEach((d) => {
                        d.tier = d.tier.toLowerCase();
                    });
                    callback(null, data);
                }else{
                    console.log('error');
                }
            });
        },

        /**
         * Function: get 10 match data of input user
         */
        (data, callback) => {
            url = `https://na1.api.riotgames.com/lol/match/v3/matchlists/by-account/${data.accountId}?beginIndex=0&endIndex=10&api_key=${api_key}`;
            request(url, (err, response, body) => {
               if(!err && response.statusCode === 200){
                   data.matches = JSON.parse(body).matches;
                   for(let i = 0; i < data.matches.length; i++){

                       /**
                        * Get game type
                        */
                       switch (data.matches[i].queue){
                           case 400:
                           case 430:
                               data.matches[i].gameType = "Normal";
                               break;
                           case 420:
                           case 440:
                           case 470:
                               data.matches[i].gameType = "Ranked";
                               break;
                           case 450:
                               data.matches[i].gameType = "ARAM";
                               break;
                           case 460:
                               data.matches[i].gameType = "3V3";
                               break;
                           case 800:
                           case 810:
                           case 820:
                           case 830:
                           case 850:
                               data.matches[i].gameType = "AI";
                               break;
                           case 1000:
                               data.matches[i].gameType = "Overcharge";
                               break;
                       }

                   }
                   callback(null, data);
               }else{
                   console.log('error');
               }
            });
        },

        /**
         * get match detail from another url
         */
        (data, callback) => {

            /**
             * Testing URl, try to get complete game information and display on page
             * Grabbing the first one for testing purpose
             */
            url = `https://na1.api.riotgames.com/lol/match/v3/matches/${data.matches[0].gameId}?api_key=${api_key}`;
            request(url, (err, response, body) => {
                if(!err && response.statusCode === 200){
                    let json = JSON.parse(body);
                    let playerlist = json.participantIdentities;
                    let player_id = 0;

                    /**
                     * Determine the position of our player
                     */
                    playerlist.forEach(player => {
                        if(player.player.accountId === data.accountId){
                            player_id = player.participantId;
                        }
                    });

                    data.matches[0].playerlist = playerlist;
                    data.matches[0].player_id = player_id;

                    /**
                     * Find the winning team
                     */
                    if(json.teams[0].win === 'win'){
                        if(player_id <= 5){
                            data.matches[0].result = "Victory";
                        }else{
                            data.matches[0].result = "Defeat";
                        }
                    }else{
                        if(player_id > 5){
                            data.matches[0].result = "Defeat";
                        }else{
                            data.matches[0].result = "Victory";
                        }
                    }

                    /**
                     * Get game duration
                     */
                    data.matches[0].gameDuration = (json.gameDuration/60).toFixed(2);

                    /**
                     *  Get Summoner Spell Ids
                     */
                    data.matches[0].spell1Id = json.participants[player_id - 1].spell1Id;
                    data.matches[0].spell2Id = json.participants[player_id - 1].spell2Id;


                    callback(null, data);
                }else{
                    console.log('error');
                }
            });
        },

        /**
         * Get champion used by our player
         */
        (data, callback) => {

            url = `https://na1.api.riotgames.com/lol/static-data/v3/champions/${data.matches[0].champion}?locale=en_US&api_key=${api_key}`;
            request(url, (err, response, body) => {
                if (!err && response.statusCode === 200) {
                    let json = JSON.parse(body);
                    data.matches[0].Champion_key = json.key;
                    data.matches[0].Champion = json.name;
                    callback(null, data)
                } else {
                    console.log("Champion Error");
                }
            });
        },

        /**
         * Get summoner spell name and keys
         */
        (data, callback) => {

            url = `https://na1.api.riotgames.com/lol/static-data/v3/summoner-spells/${data.matches[0].spell1Id}?locale=en_US&api_key=${api_key}`;
            request(url, (err, response, body) => {
                if (!err && response.statusCode === 200) {
                    let json = JSON.parse(body);
                    data.matches[0].spell1_key = json.key;
                    data.matches[0].spell1_name = json.name;
                    callback(null, data)
                } else {
                    console.log("Summoner Spell Error");
                }
            });
        }


    ], (err, json) => {
       if(err || _.isEmpty(json)){
           res.render('empty')
       }else{
           res.render('partials/main', {
               info: json
           });
       }
    });
});


let port = Number(process.env.PORT || 3000);
app.listen(port);